from flask import Flask, request, jsonify
import logging

app = Flask(__name__)
logging.basicConfig(level=logging.INFO)

@app.route('/post', methods=['POST'])
def main():
    logging.info(f'Request: {request.json!r}')
    response = {
        'session': request.json['session'],
        'version': request.json['version'],
        'response': {
            'end_session': False
        }
    }
    handle_dialog(request.json, response)
    logging.info(f'Response:  {response!r}')
    return jsonify(response)


def handle_dialog(req, res):
    valid_languages = ["en", "fr", "ru"]
    answer = None
    if 'request' in req and 'original_utterance' in req['request']:
        answer = req['request']['original_utterance'].lower().strip()
    if req['session']['new'] or answer not in valid_languages:
        res['response']['text'] = 'Select answer'
        res['response']['buttons'] = [
            {'title': lang, 'hide': True} for lang in valid_languages
        ]
        return
    if answer == 'en':
        res['response']['text'] = 'Hello'
        res['response']['end_session'] = True
        return
    if answer == 'fr':
        res['response']['text'] = 'Bonjour'
        res['response']['end_session'] = True
        return
    if answer == 'ru':
        res['response']['text'] = ''
        res['response']['end_session'] = True
        res['response']['card'] = {
            "image_id": "235c6e73-cfa5-4ee5-9d36-cfa5d96b7ff8",
            "type": "BigImage",
            "title": "Матрешки",
            "description": "Для России."
        }

        return



if __name__ == '__main__':
    app.run(host='0.0.0.0')
