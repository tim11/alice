import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
console_handler.setFormatter(
    logging.Formatter('%(asctime)s %(levelname)s %(name)s %(message)s')
)
logger.addHandler(console_handler)
file_handler = logging.FileHandler('all.log', mode='w')
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(
    logging.Formatter('%(asctime)s %(levelname)s %(name)s %(message)s')
)
logger.addHandler(file_handler)
error_handler = logging.FileHandler('error.log', mode='w')
error_handler.setLevel(logging.ERROR)
error_handler.setFormatter(
    logging.Formatter('%(asctime)s %(levelname)s %(name)s %(message)s')
)
logger.addHandler(error_handler)


def log():
    logger.debug('Debug')
    logger.info('Info')
    logger.warning('Warning')
    logger.error('Error')
    logger.critical('Critical or Fatal')


if __name__ == '__main__':
    log()
